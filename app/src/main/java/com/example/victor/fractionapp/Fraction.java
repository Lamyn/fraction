package com.example.victor.fractionapp;

/**
 * Created by victor on 27.10.16.
 */
public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }

    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public Fraction add (Fraction f) {
        Fraction res = new Fraction();
        res.nominator = (f.nominator * this.denominator + f.denominator * this.nominator);
        res.denominator = (this.denominator * f.denominator);
        return res;
    }


    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction sub(Fraction f2) {
        Fraction res = new Fraction();
        res.nominator = (f2.nominator * this.denominator - f2.denominator * this.nominator);
        res.denominator = (this.denominator * f2.denominator);
        return res;
    }

    public Fraction mult(Fraction f2) {
        Fraction res = new Fraction();
        res.nominator  = f2.nominator * this.nominator;
        res.denominator = f2.denominator * this.denominator;
        return res;
    }

    public Fraction div(Fraction f2) {
        Fraction res = new Fraction();
        res.nominator = f2.nominator * this.denominator;
        res.denominator = f2.denominator * this.nominator;
        return res;
    }
}
